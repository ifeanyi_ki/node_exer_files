var http = require('http');

var options = {
  host: 'localhost',
  port: '8989',
  path: '/index.htm'
};


var callback = function(response){
  console.log("*** callback function processing ***");
  var body = '';
  response.on('data', function(data){ 
     body += data;
  });

  response.on('end', function(){
    console.log(body);
  });

  console.log("*** finished processing request***");
}

console.log("*** make request call to server ***");
var req = http.request(options, callback);

req.end;