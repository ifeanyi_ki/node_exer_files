var math = require('./modules/math');

exports.test_add = function(test){
	test.equals(math.add(1, 1), 2);
	test.done();
};

exports.test_sub = function(test){
	test.equals(math.sub(4, 2), 2);
	test.done();
};