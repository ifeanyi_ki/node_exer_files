var math = require('./modules/math');

exports['calculateadd'] = function(test){
	test.equal(math.add(1, 1), 2);
	test.done();
};

exports['calculatesub'] = function(test){
	test.equal(math.sub(4, 2), 2);
	test.done();
};