var fs = require("fs");

console.log("*** About to delete test1 folder ***");
fs.rmdir('/test/test1', function(err){  
    if(err){
       return console.error(err);
    }

    console.log("*** About to read test dir ***");

    fs.readdir('/test', function(err, files){
     
    if(err){
        return console.log(err);
    }

    files.forEach( function(file){ 
        console.log(file);
    });
});
});

