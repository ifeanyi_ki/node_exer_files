var http = require('http');
var fs = require('fs');
var url = require('url');

http.createServer( function (request, response){  
  var pathname = url.parse(request.url).pathname;

  console.log("Request for " + pathname + " received.");
  console.log("just checking out pathname>>>" + pathname.substr(1));

  fs.readFile(pathname.substr(1), function(err, data){
    if(err){ 
      console.error(err);
      response.writeHead(404, {'Content-Type' : 'text/html'});
    }else{
      response.writeHead(200, {'Content-Type' : 'text/html'});
      console.log("*****************************************");
      console.log(data.toString());
      console.log("*****************************************");
      response.write(data.toString());
      //response.write("<html><head><title>My Page</title></head><body>Hello Nody!!!</body></html>");
    }

    response.end();

  });


}).listen(8989);

console.log('Server running at http://127.0.0.1');